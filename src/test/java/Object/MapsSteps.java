package Object;


import org.openqa.selenium.By;
import org.openqa.selenium.Keys;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebElement;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.openqa.selenium.interactions.Actions;



public class MapsSteps {
    public static void main(String[] args) throws InterruptedException {

        WebDriver driver;
        driver =new FirefoxDriver();
        driver.get("https://google.cl");
        WebElement btnBuscar= driver.findElement(By.xpath("//input[@title='Buscar']"));
        Actions drivers = new Actions(driver);
        drivers.sendKeys(btnBuscar,"maps").sendKeys(btnBuscar, Keys.ENTER).build().perform();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//h3[@class='LC20lb DKV0Md'][contains(text(),'Google Maps')]")).click();
        Thread.sleep(3000);
        WebElement escribir = driver.findElement(By.xpath("//input[@aria-label='Buscar en Google Maps']"));
        escribir.sendKeys("antonio ebner 1330, quinta normal");
        escribir.sendKeys(Keys.ENTER);
        Thread.sleep(3000);
        driver.findElement(By.xpath("//img[contains(@alt,'Cómo llegar')]")).click();
        WebElement BtnDireccion = driver.findElement(By.xpath("//input[@aria-label='Elige un punto de partida o haz clic en el mapa...']"));
        Thread.sleep(2500);
        BtnDireccion.sendKeys("Isidora Goyenechea 3365, Las Condes, Región Metropolitana");
        BtnDireccion.sendKeys(Keys.ENTER);
        Thread.sleep(2000);
        driver.findElement(By.xpath("//img[contains(@data-tooltip,'Transporte público')]")).click();
        Thread.sleep(2000);
        driver.findElement(By.xpath("//div[@class='section-directions-trip-summary']")).click();
        Thread.sleep(5000);
        driver.quit();
    }
}

